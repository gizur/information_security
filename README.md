Information security
===================

This repository contains the information security policies to be used by Gizur employees, sub-contractors and vendors.


General policies
----------------

Mails must be encrypted with PGP. For instance can  [GPG Tools](https://gpgtools.org) be used on Mac and [GPG4Win](http://www.gpg4win.org) or [Enigmail](https://www.enigmail.net) on Windows. [This](http://keyserver.pgp.com) is a good place to publish your public key (export it in ascii and not binary format). Make sure to save a backup of your private key. Dropbox, a USB stick, etc. is fine since you are using a strong password, see below.
You can also encrypt it with a password that you're sure that you remember, [here](http://www.wikihow.com/Create-a-Password-You-Can-Remember) are some tips.

Always use strong passwords. Passwords should be at least 8 characters, contain upper and lower case, numbers and at least one special character. Check the strength of a password [here](https://www.grc.com/haystack.htm) (try a password that is similar to your own if you're not comfortable entering your password here).

A good way to store passwords for the internet is to use the password store in internet browsers. 
This makes it convenient to use really strong passwords. Use a Web service like [this](http://www.thebitmill.com/tools/password.html) or [this](https://secure.pctools.com/guides/password/?length=32&phonetic=on&alpha=on&mixedcase=on&numeric=on&punctuation=on&nosimilar=on&quantity=50&generate=true#password_generator) or, even better, the command line `openssl rand -base64 32` to generate strong passwords. [Safari](https://discussions.apple.com/thread/5041659?start=0&tstart=0) is safe. Use a master password in [Firefox](https://support.mozilla.org/en-US/kb/use-master-password-protect-stored-logins). [Internet Explorer](http://www.howtogeek.com/68231/how-secure-are-your-saved-internet-explorer-passwords/) should also be safe. I'm note sure about Chrome. You must of course make sure that the password the browser is protected by is strong, e.g. your Mac/Windows login or Firefox master password.




Policies for developers and system administrators
-------------------------------------------------

Developers and system administrators often have access to extra sensitive data. Extra pre-cautions must therefore be taken.

Hard drives (or partitions/file systems) containing projects and/or other sensitive data must always be encrypted. FileVault should be used on Macs and Windows has BitLocker. Different approaches can be used on Linux, Ubuntu can be setup at installation and [here](https://wiki.archlinux.org/index.php/System_Encryption_with_LUKS) is another approach.

Repositories and code must never contain sensitive data that have not been encrypted. Passwords (database credentials etc.) is typically entered when starting the procesess(es). A good way to handle this is to pass them in an environment variable accessible to the processes(es) started.


Some things that are good to know
----------------------------------

 * http://www.digitaltrends.com/mobile/can-email-ever-be-secure/#!bNdLgE
 * https://help.github.com/articles/github-security
 * https://www.dropbox.com/help/27
 * https://support.skype.com/en/faq/FA31/does-skype-use-encryption
 * http://answers.microsoft.com/en-us/onedrive/forum/sdfiles-sdother/how-secure-are-files-on-skydrive/d3ed5c9d-ea48-e011-8dfc-68b599b31bf5
 

Cheat sheet
----------

 * Suggested approach for generating passwords: `openssl rand -base64 32`.
 * Show fingerprint for private SSH key: `ssh-keygen -l -f [FILE]` (ssh keys are typically stored in ./ssh/id_rsa)
 * List the keys in your GPG Keystore: `gpg --list-keys`
 * Encrypt a file: `gpg -e -a -u [SENDER] -r [RECEIVER 1] -r [RECEIVER 2] [FILE]`. NOTE: As many recipients as you want can be used. Decrypt with : `gpg -d [FILE]` 
 * Encrypt a file with a password (less secure than public key encryption but far far better than no encryption):  `gpg -a --symmetric --cipher-algo aes256 [FILENAME]`. Decrypt like this `gpg --decrypt [FILENAME]`


Resources
---------

 * [SSH keys and Github](https://help.github.com/articles/generating-ssh-keys)
 * [SANS Information Security Policy Templtes](http://www.sans.org/security-resources/policies/)
 * [10 ways to generate random passwords](http://www.howtogeek.com/howto/30184/10-ways-to-generate-a-random-password-from-the-command-line/)
 * [Encrypt email in Outlook and Gmail](http://www.triatechnology.com/sending-a-pgp-encrypted-email-in-outlook-or-webmail/)
 * [PGP for Webmail](https://www.mailvelope.com)
 * [GPG Cheat Sheet](http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/gpg-cs.html)
 * [CERN](https://security.web.cern.ch/security/rules/en/index.shtml)
 * [Nice infographics](http://www.clubcloudcomputing.com/2013/01/infographic-on-hacking-statistics/)
 * [Hacker News](http://thehackernews.com)
